gridtracker (1.22.0503) unstable; urgency=low
  - Increment version for build with correct NWJS version
  
  -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 01 May 2022 16:07:00 -0000
  
gridtracker (1.22.0502) unstable; urgency=low
  [Bug Fixes]
  - Fixed broken Call Roster due to online assets being moved from a web server to Google Storage Bucket.
  - Don't highlight "CQ" rows if filtering by "CQ Only".
  - Resolved #126 Windows Installer script updated to fix issues with install location and missing registry keys
  - Resolved #124 removing IP-Geolocation when no all other means of locating failed, we now tell the user to
    start WSJT-X or enter a location  as Geo-Location services are costly and unreliable
  - Resolved #137 missing libatomic dependency in Linux DEB and RPM spec files
  [Enhancements]
  - Include version number in main window title
  - Call Roster colums refactored and wanted column added
  
 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 01 May 2022 02:25:40 -0000

gridtracker (1.21.1217) unstable; urgency=low
  - Changed to newer NWJS to fix upstream bug that caused media playback to fail.

 -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 17 Dec 2021 20:08:00 -0000
 
gridtracker (1.21.1212) unstable; urgency=low
  Release build with the call roster refactor code that's been in the works for some time.
  [Bug Fixes]
  - Fix #76, unfinished ignore CQ and ITU zones.
  - Improved handling of stations that are not in a valid DXCC (ie; /MM stations)
  - Improved handling of free text decodes that don't contain valid callsigns (ie "HI BOB" and "MERRY XMAS")
  - Fix how the Call Roster title bar counts are calculated.
  [Enhancements]
  - More clarity when a ULS Zip code falls in more then one county, replacing ~ with ? symbols and
    better tool tip message.
  - Fix #107, where the call roster timeout was longer then a single FT4 cycle.
  - Fix #91, CQ is always highlighted, no matter status of CQ Only.
  - Performance improvement by changing how call roster vars are handled ('let' vs 'var')
  - Build system improved to push to Arch AUR, building of Debian (.deb) packages and triggering
    of COPR RPM builds for Fedora/Cent/RHEL and their cousins.

 -- Matthew Chambers <nr0q@gridtracker.org>  Thu, 12 Dec 2021 15:10:00 -0000

gridtracker (1.21.0928) unstable; urgency=medium
  [Bug Fixes]
  - Treat ADIF record values as byte length vs string length (to better handle UTSF-8 data).
  - Remove looking at fetched records for last date for LoTW fetches, Use only headers (More reliable LoTW fetches).
  [Enhancements]
  - ARM builds now with NWJS 0.54.2 and 64 bit ARM binaries.

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 28 Sep 2021 00:00:00 -0000

gridtracker (1.21.0620) unstable; urgency=medium
  [Bug Fixes]
  - Fix pulling down of LoTW logs at start-up with a differential log syncing mechanism that only get's changes since last sync, also cool down timer to prevent rapid reloading of LoTW log. 
  [Enhancements]
  - Automatic pulling down of acknowledgements.json file daily when doing version check (if enabled). 

 -- Matthew Chambers <nr0q@gridtracker.org>  Sat, 19 Jun 2021 16:49:00 -0000
 
gridtracker (1.21.0613) unstable; urgency=medium
  [Bug Fixes]
  - Fix pulling down LoTW log at start-up causing issue for other programs that are trying to sync LoTW logs.
  [Enhancements]
  - Updated list of contributors

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 13 Jun 2021 03:04:00 -0000

gridtracker (1.21.0530) unstable; urgency=medium
  [Bug Fixes]
  - Fix spots layer not refreshing/clearing when TX idle
  - Mark /MM as not DXCC per ARRL DXCC rules
  - Fix bug that causes call roster to stick
  - Fix typo in Call Roster "OAMS" heading
  - Fix DXCC GeoJSON centers that broke DXCC based map pathes
  - Fix mislabel of Eswatini
  [Enhancements]
  - Add statistical information to call roster title bar

 -- Matthew Chambers <nr0q@gridtracker.org>  Sat, 30 May 2021 00:10:00 -0000

gridtracker (1.21.0520) unstable; urgency=medium
  [Bug Fixes]
  - Fix PSK Reporter poll time to 5min , add TX idle timeout and time skew to reduce the load GridTracker makes on the PSK Reporter server
  - Fix SVG icons not rendering on certain Windows 10 installs
  - Fix eQSL ADIF support
  - Fix clearing of logs after installing new version of GridTracker
  - Fix hightlighting whole country of Somalia on map
  [Enhancements]
  - Update Award Tracker with new FT8DMC and European ROS Club awards
  - Improvements to the callroster
  - Add recognition of contributors to GridTracker within the Call Roster and lookup window
  - Make settings icon a toggle that both opens and closes the settings pane
  - Grid and IP Address Fields are slightly wider

 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 17 May 2021 02:30:00 -0000

gridtracker (1.21.0407) unstable; urgency=medium
  [Bug Fixes]
  - mp3 alerts from previous versions now work correctly
  - callook lookup preference now stored
  - call roster WSJT-X/JTDX instance label/checkbox overlap fixed
  [Enhancements]
  - new icon to request ClubLog OQRS QSL
  - add eQSL check in log file processing

 -- Matthew Chambers <nr0q@gridtracker.org>  Wed, 07 Apr 2021 00:00:00 -0000

gridtracker (1.21.0327) unstable; urgency=medium
  This is the public release of the 1.21.0324 hotfix release candidates

 -- Matthew Chambers <nr0q@gridtracker.org>  Fri, 27 Mar 2021 00:38:00 -0000

gridtracker (1.21.0324) unstable; urgency=medium
  [Christian Bayer]
  * Fixed #72 CR not filtering correctly
  * Fixed #63 blurry Windows icon
  [Matthew Chambers]
  * Fixed #71 bug with loading adif files
  This is the hotfix release of the first public release of 1.21.0322

 -- Christian Bayer <chrbayer84@googlemail.com>  Wed, 22 Mar 2021 22:00:00 -0500

gridtracker (1.21.0322) unstable; urgency=medium
  [Christian Bayer]
  * Fixed windows packaging 
  [Matthew Chambers]
  * Fixed linux and arm packaging
  This is the public release of 1.21.0307 release candidates

 -- Matthew Chambers <nr0q@gridtracker.org>  Mon, 22 Mar 2021 20:30:00 -0500

gridtracker (1.21.0307) unstable; urgency=medium

  [ Paul Traina ]
  * Cleaned up the title bar to show context information
  * GT now recognizes JTDX on Mac
  * PSK-Spot layer merged with Live/Logbook Grids by default
  * Inputs in settings sanitized
  * Fix spots when in realtime mode
  * Added CQ zone name in reports
  [ Tag Loomis ]
  * Roster context menu active on entire window
  * remote station distance and azimuth in lookup if available
  [ Sebastian Delmont ]
  * Changed layout of roster controls and made it simpler to hide and show them
  * Allow roster window to be as narrow as WSJT-X narrowest window size
  [ Mattew Chambers ]
  * Call Roster is now labeled "GridTracker" and contains partial layer
    information
  [ Christian Bayer ]
  * Fixed ordering of centimeter bands in stats window
  * Clicking an unconfirmed DXCC in stats window now shows relevant log entries
    in pop up window
  * New Callook preference setting. if checked, US callsigns will always be
    queried from Callook since it usually has more data than free QRZ lookup
  * fixed CR alert script not being triggered for Awared Tracker hits

 -- Matthew Chambers <nr0q@gridtracker.org>  Sun, 7 Mar 2021 12:00:00 -0000

gridtracker (1.20.1118) unstable; urgency=low

  * GridTracker is now Open Source! Copyright assigned to GridTracker.org and
    is BSD 3-clause.
  [ Paul Traina ]
  * Disconnect from tagloomis.com infrastructure -> gridtracker.org
  * Disable fit to QTH when in PSK mode.
  * Highlight confirmed DXCC countries in DXCC report.
  * Unify worked/confirmed/unworked in WAC/WAS/CQ Zones/ITU Zones reports.
  * In call roster, when requiring LoTW, don't show stations that don't meet
    time limits.
  * Improve call roster title bar.
  * Fix duplicate-first-name in lookup window.
  * Change ADIF COUNTRY field output to comply with ADIF 3.1.1 specification.
  * Clean up media handling.
    Don't create duplicates of GT's media files in Documents/GridTracker/media.
    Remove any duplicates that are already there.
    User directory is still respected if you want to add your own files there.
  * Add a man page for GridTracker for Linux packages.
  * Add RPM build support, based upon NR0Q's work.
  * Support auto-building CI/CD for GitLab, Debian, and RPM packaging
  [ Sebastian Delmont ]
  * Improve roster controls
  * Clean up and pretify HTML and JSON code, reformat code base.
  * Make UDP port vaildation behavior clearer, allow receive on 2238 if
    forwarding enabled.
  * Improve debugging/developer experience by enabling context menus when
    using nwjs's SDK.

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Wed, 04 Nov 2020 15:58:29 -0800

gridtracker (1.20.0927+repack1) unstable; urgency=high

  * Clean up nw execution in .desktop and .sh file.

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Wed, 07 Oct 2020 09:16:02 -0700

gridtracker (1.20.0927) unstable; urgency=medium

  [ Tag Loomis ]
  * Author release 1.20.0927

  [ Paul Traina ]
  * Debian package building support

 -- Paul Traina <216482-pleasantone@users.noreply.gitlab.com>  Mon, 28 Sep 2020 14:06:49 -0700
